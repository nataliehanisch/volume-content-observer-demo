package space.lona.natalie.volumecontentobserverdemo;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String message = "App Initialized\n";
    private int count = 0;
    private Handler mHandler;
    private VolumeService mService = null;
    private static MainActivity instance;
    private boolean mIsBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        TextView textView = findViewById(R.id.textView);
        textView.setText(message);

        Looper looper = Looper.getMainLooper();
        mHandler = new Handler(looper);

        Intent intent = new Intent(MainActivity.this, VolumeService.class);
        startService(intent);
        doBindService();

    }

    public void updateText(String text) {

        message += "Event " + count + ":\t" + text + "\n";

        TextView textView = findViewById(R.id.textView);
        textView.setText(message);

        count += 1;

    }

    public static MainActivity getInstance() {
        return instance;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            mService = ((VolumeService.LocalBinder)iBinder).getInstance();
            mService.setHandler(mHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            mService = null;
        }
    };

    private void doBindService() {
        bindService(new Intent(this,
                VolumeService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    private void doUnbindService() {
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        doUnbindService();
    }

}
