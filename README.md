# Volume Content Observer Demo

Regarding Android application development: This is a quick demo for how to use a content observer in a background service to track volume changes in your app. I have not done detailed work with background services, and I suspect that it may be resource-greedy. In the future I will push changes to resolve that and any other issues I may find.

## Compatibility

At the time of this posting, the app works just fine on a Google Pixel 1 running Android 9. 