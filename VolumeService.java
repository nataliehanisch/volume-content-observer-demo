package space.lona.natalie.volumecontentobserverdemo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;

public class VolumeService extends Service {

    private AudioManager mAudioManager;
    private Handler mHandler = null;
    private SettingsObserver mVolumeObserver;
    private final IBinder mIBinder = new LocalBinder();

    class SettingsObserver extends ContentObserver {

        Context context;

        SettingsObserver(Context c, Handler handler) {

            super(handler);
            context = c;
            mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            // Be sure to check out all the possible audio streams that you could be adjusting
            MainActivity.getInstance().updateText("Music volume " + Integer.toString(
                    mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)));

        }
    }

    @Override
    public void onCreate() {

        super.onCreate();

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mVolumeObserver = new SettingsObserver(this, new Handler());
        getApplicationContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, mVolumeObserver);

        MainActivity.getInstance().updateText("Broadcast Receiver Started");

    }

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mIBinder;
    }

    public class LocalBinder extends Binder {
        public VolumeService getInstance() {
            return VolumeService.this;
        }
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

}
